package INF101.lab2;
import java.util.*;  


public class Fridge implements IFridge {
    
    ArrayList<FridgeItem> fridge = new ArrayList<FridgeItem>();
    int max_items = 20;
    
    
    /** returns how many items in fridge */
    public int nItemsInFridge() {
        return fridge.size();
    }
    /** returns how many items there are space for in the fridge */
    public int totalSize() {
        return max_items;
    }
    /** place item in fridge if the frige is not full */
    @Override
    public boolean placeIn(FridgeItem item) {
        
        if (fridge.size() < max_items){
            fridge.add(item);
            return true;
        }
        else {
            return false;
        }
    }
    /** take out item, if it is in the fridge */
    @Override
    public void takeOut(FridgeItem item) {
        if(fridge.contains(item)) {
			fridge.remove(item);
		}
		else {
			throw new NoSuchElementException("The fridge does not contain this item");
		}
    }
    /** empty the whole frige if there is something in it */
    @Override
    public void emptyFridge() {
        if(fridge.isEmpty()) {
			throw new NoSuchElementException("Can not remove items from an empty fridge");
		}
		else {
		fridge.clear();
		}

    }
    //removes expired food and give it as a list
    @Override
    public List<FridgeItem> removeExpiredFood() {

        ArrayList<FridgeItem> tmp = new ArrayList<FridgeItem>();

        for(FridgeItem i : fridge) {
			if(i.hasExpired()) {
                tmp.add(i);
				
			}
		}
        for(FridgeItem i : tmp) {
            if(i.hasExpired()){
                fridge.remove(i);

            }
        }
        return tmp;  
	}
}